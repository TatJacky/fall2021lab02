//Jacky Tat 1911748
public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bicycleArray = new Bicycle[4];
        bicycleArray[0] = new Bicycle("Trek", 19, 34);
        bicycleArray[1] = new Bicycle("Connondale", 22, 43);
        bicycleArray[2] = new Bicycle("Kona", 21, 45);
        bicycleArray[3] = new Bicycle("Colnago", 24, 44);

        for(Bicycle x: bicycleArray){
            System.out.println(x);
        }
    }
}
