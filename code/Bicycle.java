//Jacky Tat 1911748
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //Constructor
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //Set Methods
    public void setManufacturer(String newManufacturer){
        this.manufacturer = newManufacturer;
    }

    public void setNumberGears(int newNumberGears){
        this.numberGears = newNumberGears;
    }

    public void setMaxSpeed(double newMaxSpeed){
        this.maxSpeed = newMaxSpeed;
    }

    //Get Methods
    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    @Override
    public String toString() {
        return "Manufacturer: "+ this.manufacturer+
        ", Number of Gears: "+ this.numberGears+
        ", MaxSpeed: "+ this.maxSpeed;
    }
}